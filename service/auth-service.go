package service

import (
	"github.com/mashingan/smapping"
	"golang.org/x/crypto/bcrypt"
	"latihan-gin-jwt/dto"
	"latihan-gin-jwt/entity"
	"latihan-gin-jwt/repository"
	"log"
)

type AuthService interface {
	VerifyCredential(email,password string) interface{}
	CreateUser(user dto.RegisterDTO) entity.User
	FindByEmail (email string) entity.User
	IsDuplicateEmail(email string) bool
}

type authService struct {
	userRepository repository.UserRepository
}

func NewAuthService(userMap repository.UserRepository) AuthService {
	return &authService{userRepository: userMap}
}

func (service *authService) VerifyCredential(email, password string) interface{} {
	res := service.userRepository.VerifyCredential(email,password)
	if verif,ok := res.(entity.User); ok{
		comparedPassword := comparedPassword(verif.Password, []byte(password))
		if verif.Email == email && comparedPassword {
			return res
		}
		return false
	}
	return false
}

func (service *authService) CreateUser(user dto.RegisterDTO) entity.User{
	userToCreate := entity.User{}
	err := smapping.FillStruct(&userToCreate, smapping.MapFields(&user))
	if err != nil {
		log.Fatal("failed map %v", err)
	}
	res := service.userRepository.InsertUser(userToCreate)
	return res
}

func (service *authService) FindByEmail(email string) entity.User {
	return service.userRepository.FindByEmail(email)
}

func (service *authService) IsDuplicateEmail(email string) bool {
	res := service.userRepository.IsDuplicateEmail(email)
	return (res.Error == nil)
}

func comparedPassword(hashedPassword string, plainPassowrd []byte) bool {
	byteHash := []byte(hashedPassword)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPassowrd)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}