package main

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"latihan-gin-jwt/config"
	"latihan-gin-jwt/controller"
	"latihan-gin-jwt/middleware"
	"latihan-gin-jwt/repository"
	"latihan-gin-jwt/service"
)

var (
	db *gorm.DB = config.SetupDatabaseConnection()
	userRepository repository.UserRepository = repository.NewUserRepository(db)
	bookRepository repository.BookRepository = repository.NewBookRepository(db)
	jwtService service.JWTService = service.NewJWTService()
	userService service.UserService = service.NewUserService(userRepository)
	bookService service.BookService = service.NewBookService(bookRepository)
	authService service.AuthService = service.NewAuthService(userRepository)
	authController controller.AuthController = controller.NewAuthController(authService, jwtService)
	userController controller.UserController = controller.NewUserController(userService,jwtService)
	bookController controller.BookController = controller.NewBookController(bookService,jwtService)
)
func main() {
	defer config.CloseDatabaseConnection(db)
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	authRoutes := r.Group("rest/open")
	{
		authRoutes.POST("/login", authController.Login)
		authRoutes.POST("/register", authController.Register)

	}

	userRoutes := r.Group("rest/secured/user", middleware.AuthorizeJWT(jwtService))
	{
		userRoutes.GET("/profile", userController.Profile)
		userRoutes.PUT("/profile", userController.Update)
	}

	bookRoutes := r.Group("rest/secured/book", middleware.AuthorizeJWT(jwtService))
	{
		bookRoutes.GET("/", bookController.FindAll)
		bookRoutes.GET("/:id", bookController.FindByID)
		bookRoutes.POST("/",bookController.Insert)
		bookRoutes.PUT("/:id",bookController.Update)
		bookRoutes.DELETE("/:id", bookController.Delete)
	}
	r.Run()
}
