package dto

type RegisterDTO struct {
	Name string `json:"name" from:"name" binding:"required,min=1"`
	Email string `json:"email" from:"email" binding:"required,email"`
	Password string `json:"password" from:"password" binding:"required,min=6"`
}
