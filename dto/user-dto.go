package dto

type UserUpdateDTO struct {
	ID uint64 `json:"id" from:"id"`
	Name string `json:"name" from:"name" binding:"required"`
	Email string `json:"email" from:"email" binding:"required,email"`
	Password string `json:"password" from:"password" binding:"required"`
}

//type UserCreateDTO struct {
//	Name string `json:"name" from:"name" binding:"required"`
//	Email string `json:"email" from:"email" binding:"required" validate:"email"`
//	Password string `json:"password" from:"password" validate:"min:6" binding:"required"`
//}
