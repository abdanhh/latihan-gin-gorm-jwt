package config

import (
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"latihan-gin-jwt/entity"
	"os"
)

func SetupDatabaseConnection() *gorm.DB {
	err := godotenv.Load()
	if err != nil {
		panic("Failed to load env file")
	}
		DbUser := os.Getenv("DB_USER")
		DbPassword := os.Getenv("DB_PASSWORD")
		DbHost := os.Getenv("DB_HOST")
		DbName := os.Getenv("DB_NAME")
		DbPort := os.Getenv("DB_PORT")

		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
		db, err := gorm.Open(postgres.Open(DBURL), &gorm.Config{})

		if err != nil {
			panic("Failed to create connection to database")
		}
		db.AutoMigrate(&entity.User{}, &entity.Book{})
	return db
}

func CloseDatabaseConnection(db *gorm.DB) {
	dbSQL, err := db.DB()
	if err != nil {
		panic("Failed to close connection from database")
	}
	dbSQL.Close()
}
