package controller

import (
	"github.com/gin-gonic/gin"
	"latihan-gin-jwt/dto"
	"latihan-gin-jwt/entity"
	"latihan-gin-jwt/helper"
	"latihan-gin-jwt/service"
	"net/http"
	"strconv"
)

type AuthController interface {
	Login(ctx *gin.Context)
	Register(ctx *gin.Context)
}

type authController struct {
	authService service.AuthService
	jwtService service.JWTService
}

func NewAuthController(authService service.AuthService, jwtService service.JWTService) AuthController {
	return &authController{
		authService: authService,
		jwtService: jwtService,
	}
	
}

func (c *authController) Login(ctx *gin.Context) {
	var loginDto dto.LoginDTO
	err := ctx.ShouldBind(&loginDto)
	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest,response)
		return
	}
	authResult := c.authService.VerifyCredential(loginDto.Email,loginDto.Password)
	if v,ok := authResult.(entity.User);ok{
		generatedToken := c.jwtService.GenerateToken(strconv.FormatUint(v.ID,10))
		v.Token = generatedToken
		response := helper.BuildResponse(true,"OK",v)
		ctx.JSON(http.StatusOK, response)
		return
	}
	response:= helper.BuildErrorResponse("Please check again your credential","Invalid Credential", helper.EmptyObj{})
	ctx.AbortWithStatusJSON(http.StatusUnauthorized,response)
}

func (c *authController) Register(ctx *gin.Context) {
	var registerDto dto.RegisterDTO
	err := ctx.ShouldBind(&registerDto)
	if err != nil {
		response := helper.BuildErrorResponse("failed to procces request",err.Error(),helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest,response)
		return
	}
	if c.authService.IsDuplicateEmail(registerDto.Email){
		response := helper.BuildErrorResponse("Failed to process request", "Duplicate email",helper.EmptyObj{})
		ctx.JSON(http.StatusConflict, response)
	} else {
		createdUser := c.authService.CreateUser(registerDto)
		token := c.jwtService.GenerateToken(strconv.FormatUint(createdUser.ID,10))
		createdUser.Token = token
		response := helper.BuildResponse(true,"OK", createdUser)
		ctx.JSON(http.StatusCreated,response)
	}
}