package repository

import (
	"gorm.io/gorm"
	"latihan-gin-jwt/entity"
)

type BookRepository interface {
	InsertBook(b entity.Book) entity.Book
	UpdateBook(b entity.Book) entity.Book
	DeleteBook(b entity.Book)
	FindAll() []entity.Book
	FindBookByID(bookID uint64) entity.Book
}

type bookConnection struct {
	bookConnection *gorm.DB
}

func NewBookRepository(db *gorm.DB) BookRepository {
	return &bookConnection{
		bookConnection: db,
	}
}

func (db *bookConnection) InsertBook(b entity.Book) entity.Book{
	db.bookConnection.Save(&b)
	db.bookConnection.Preload("User").Find(&b)
	return b
}

func (db *bookConnection) UpdateBook(b entity.Book) entity.Book {
	db.bookConnection.Save(&b)
	db.bookConnection.Preload("User").Find(&b)
	return b
}

func (db *bookConnection) DeleteBook(b entity.Book) {
	db.bookConnection.Delete(&b)
}

func (db *bookConnection) FindBookByID(bookID uint64) entity.Book {
	var book entity.Book
	db.bookConnection.Preload("User").Find(&book, bookID)
	return book
}

func (db *bookConnection) FindAll() []entity.Book {
	var books []entity.Book
	db.bookConnection.Preload("User").Find(&books)
	return books
}